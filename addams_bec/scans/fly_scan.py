
import numpy

from bec_lib import messages
from bec_lib.endpoints import MessageEndpoints
from bec_lib.devicemanager import DeviceManagerBase
from bec_lib.logger import bec_logger
from bec_server.scan_server.scans import ScanArgType, ScanBase, AsyncFlyScanBase
from bec_server.scan_server.errors import ScanAbortion

from bec_lib.device import DeviceBase, Positioner

logger = bec_logger.logger


class HklFlyScan(AsyncFlyScanBase):
    scan_name = 'hkl_flyscan'
    scan_type = 'fly'
    arg_input = {
        'diffractometer': ScanArgType.DEVICE,
        'controller': ScanArgType.DEVICE,
        'start': ScanArgType.LIST,
        'stop': ScanArgType.LIST,
        'points': ScanArgType.INT
    }
    arg_bundle_size = {"bundle": len(arg_input), "min": 1, "max": None}
    required_kwargs = []
    use_scan_progress_report = False

    def __init__(self, diffract:Positioner, controller:DeviceBase, start:list[float], stop:list[float], points:int, optimize_profile=True, **kwargs):
        self.diffract = diffract
        self.controller = controller
        self.start = start
        self.stop = stop
        self.points = points
        self.optimize_profile = optimize_profile
        super().__init__(**kwargs)

        self.scan_report_devices = ['h', 'k', 'l'] + self.scan_motors + self.readout_priority['monitored']

    @property
    def monitor_sync(self):
        return self.diffract

    def update_scan_motors(self):
        self.scan_motors = self.device_manager.devices[self.diffract].real_axes.get()

    def prepare_positions(self):
        """
        Override base method to yield from _calculate_position method
        """
        yield from self._calculate_positions()
        self._optimize_trajectory()
        self.num_pos = len(self.positions) * self.burst_at_each_point
        yield from self._set_position_offset()
        self._check_limits()

    def _calculate_positions(self):
        hkls = numpy.linspace(self.start, self.stop, self.points).tolist()
        positions = yield from self.stubs.send_rpc_and_wait(self.diffract, 'angles_from_hkls', hkls)
        # the last two positions 'betaIn' and 'betaOut' are not real motors
        self.positions = []
        for position in positions:
            self.positions.append(position[:-2])

    def scan_core(self):
        scan_motors = self.device_manager.devices[self.diffract].real_axes.get()
        hkls = numpy.linspace(self.start, self.stop, self.points).tolist()
        positions = yield from self.stubs.send_rpc_and_wait(self.diffract, 'angles_from_hkls', hkls)
        positions = numpy.array(positions)
        
        num_points = len(positions)
        total_time = num_points * self.exp_time

        if self.optimize_profile:
            yield from self.stubs.send_rpc_and_wait(self.controller, 'num_points.put', 2)
            yield from self.stubs.send_rpc_and_wait(self.controller, 'num_pulses.put', num_points)
            yield from self.stubs.send_rpc_and_wait(self.controller, 'start_pulses.put', 1)
            yield from self.stubs.send_rpc_and_wait(self.controller, 'end_pulses.put', 2)
            yield from self.stubs.send_rpc_and_wait(self.controller, 'time_mode.put', 1)
            yield from self.stubs.send_rpc_and_wait(self.controller, 'times.put', [total_time, total_time])

            for index, axis_name in enumerate(scan_motors):
                yield from self.stubs.send_rpc_and_wait(self.controller,
                                                    f'{axis_name}.positions.put',
                                                    (positions[0, index], positions[-1, index]))
                yield from self.stubs.send_rpc_and_wait(self.controller, f'{axis_name}.use_axis.put', 1)

        else:
            yield from self.stubs.send_rpc_and_wait(self.controller, 'num_points.put', num_points)
            yield from self.stubs.send_rpc_and_wait(self.controller, 'num_pulses.put', num_points)
            yield from self.stubs.send_rpc_and_wait(self.controller, 'start_pulses.put', 1)
            yield from self.stubs.send_rpc_and_wait(self.controller, 'end_pulses.put', num_points)
            yield from self.stubs.send_rpc_and_wait(self.controller, 'time_mode.put', 0)
            yield from self.stubs.send_rpc_and_wait(self.controller, 'fixed_time.put', self.exp_time)

            for index, axis_name in enumerate(scan_motors):
                yield from self.stubs.send_rpc_and_wait(self.controller,
                                                    f'{axis_name}.positions.put',
                                                    positions[:, index])
                yield from self.stubs.send_rpc_and_wait(self.controller, f'{axis_name}.use_axis.put', 1)

        yield from self.stubs.send_rpc_and_wait(self.controller, 'build_profile')
        build_status = yield from self.stubs.send_rpc_and_wait(self.controller, 'build_status.get')
        if build_status != 1:
            raise ScanAbortion('Profile build failed')

        yield from self.stubs.send_rpc_and_wait(self.controller, 'execute_profile')
        execute_status = yield from self.stubs.send_rpc_and_wait(self.controller, 'execute_status.get')
        if  execute_status != 1:
            raise ScanAbortion('Profile execute failed')

        yield from self.stubs.send_rpc_and_wait(self.controller, 'readback_profile')
        readback_status = yield from self.stubs.send_rpc_and_wait(self.controller,'readback_status.get')
        if readback_status != 1:
            raise ScanAbortion('Profile readback failed')

        angle_readbacks = []
        for index, axis_name in enumerate(scan_motors):
            readbacks = yield from self.stubs.send_rpc_and_wait(self.controller, f'{axis_name}.readbacks.get')
            print(axis_name, readbacks)
            self._publish_readbacks(axis_name, readbacks)
            angle_readbacks.append(readbacks)
        # motor readbacks are aranged column-wise
        angle_readbacks = [list(x) for x in zip(*angle_readbacks)]

        hkls = yield from self.stubs.send_rpc_and_wait(self.diffract, 'hkls_from_angles', angle_readbacks)
        hkls = numpy.array(hkls)

        for index, name in enumerate(['h', 'k', 'l', 'betaIn', 'betaOut']):
            self._publish_readbacks(name, hkls[:, index])

        # motor readbacks have more points than generated pulses
        self.num_pos = len(angle_readbacks)

        logger.success(f'{self.scan_name} finished')

    def _publish_readbacks(self, device, readbacks):
        metadata = {"async_update": "append", "num_lines": len(readbacks)}
        msg = messages.DeviceMessage(
            signals={device: {'value': readbacks} }, metadata=metadata
        )

        self.stubs.connector.xadd(
            topic=MessageEndpoints.device_async_readback(
                scan_id=self.scan_id, device=device
            ),
            msg_dict={"data": msg},
            expire=1800,
        )
