"""
SCAN PLUGINS

All new scans should be derived from ScanBase. ScanBase provides various methods that can be customized and overriden
but they are executed in a specific order:

- self.initialize                        # initialize the class if needed
- self.read_scan_motors                  # used to retrieve the start position (and the relative position shift if needed)
- self.prepare_positions                 # prepare the positions for the scan. The preparation is split into multiple sub fuctions:
    - self._calculate_positions          # calculate the positions
    - self._set_positions_offset         # apply the previously retrieved scan position shift (if needed)
    - self._check_limits                 # tests to ensure the limits won't be reached
- self.open_scan                         # send an open_scan message including the scan name, the number of points and the scan motor names
- self.stage                             # stage all devices for the upcoming acquisiton
- self.run_baseline_readings             # read all devices to get a baseline for the upcoming scan
- self.pre_scan                          # perform additional actions before the scan starts
- self.scan_core                         # run a loop over all position
    - self._at_each_point(ind, pos)      # called at each position with the current index and the target positions as arguments
- self.finalize                          # clean up the scan, e.g. move back to the start position; wait everything to finish
- self.unstage                           # unstage all devices that have been staged before
- self.cleanup                           # send a close scan message and perform additional cleanups if needed
"""
import time

import numpy

# from bec_lib.endpoints import MessageEndpoints
from typing import Literal
from bec_lib.devicemanager import DeviceManagerBase
from bec_lib.logger import bec_logger
# from bec_lib import messages
# from bec_server.scan_server.errors import ScanAbortion
from bec_server.scan_server.scans import RequestBase, ScanArgType, ScanBase

logger = bec_logger.logger

class HklScan(ScanBase):
    scan_name = 'hkl_scan'
    required_kwargs = ['exp_time']
    arg_input = {
        'diffractometer': ScanArgType.DEVICE,
        'start': ScanArgType.LIST,
        'stop': ScanArgType.LIST,
        'points': ScanArgType.INT
    }
    arg_bundle_size = {"bundle": len(arg_input), "min": 1, "max": None}

    def __init__(self, diffract, start, stop, points, **kwargs):
        self.diffract = diffract
        self.start = start
        self.stop = stop
        self.points = points
        super().__init__(**kwargs)

        self.scan_report_devices = ['h', 'k', 'l'] + self.scan_motors + self.readout_priority['monitored']

    def update_scan_motors(self):
        self.scan_motors = self.device_manager.devices[self.diffract].real_axes.get()

    def prepare_positions(self):
        """
        Override base method to yield from _calculate_position method
        """
        yield from self._calculate_positions()
        self._optimize_trajectory()
        self.num_pos = len(self.positions) * self.burst_at_each_point
        yield from self._set_position_offset()
        self._check_limits()

    def _calculate_positions(self):
        hkls = numpy.linspace(self.start, self.stop, self.points).tolist()
        positions = yield from self.stubs.send_rpc_and_wait(self.diffract, 'angles_from_hkls', hkls)
        # the last two positions 'betaIn' and 'betaOut' are not real motors
        self.positions = []
        for position in positions:
            self.positions.append(position[:-2])
