import builtins
import collections
import functools
import json
import math
import pathlib

import numpy
from bec_ipython_client.main import BECClientPrompt

from bec_ipython_client.prettytable import PrettyTable

__all__ = [
    'setlat',
    'setlambda',
    'setmode',
    'freeze',
    'unfreeze',
    'br',
    'ubr',
    'mvhkl',
    'umvhkl',
    'ca',
    'wh',
    'pa',
    'orientAdd',
    'orientRemove',
    'orientShow',
    'orientFit',
    'ct'
]

bec = builtins.__dict__.get("bec")
dev = builtins.__dict__.get("dev")
scans = builtins.__dict__.get("scans")

class BECClientPromptDiffractometer(BECClientPrompt):

    @property
    def username(self):
        """current username"""
        if "x04v" in dev:
            return "x04v"
        if "x04h" in dev:
            return "x04h"
        return "demo"

bec._ip.prompts = BECClientPromptDiffractometer(ip=bec._ip, username="demo", client=bec._client, status=1)


# check for diffractometer device
diffract = None
if dev is not None:
    if 'x04h' in dev:
        diffract = dev.x04h
    elif 'x04v' in dev:
        diffract = dev.x04v

if diffract is not None:
    RealPosition = collections.namedtuple('RealPosition', ' '.join(diffract.get_real_positioners()))


def freeze(
    angle: float | None
):
    """
    Freeze the value of the mode dependent angle, so when calculating motor positions 
    corresponding to an arbitrary (H, K, L ), the angle will be reset to the frozen value 
    before the calculation no matter what the current position of the diffractometer.
    """
    diffract.freeze(angle)

def unfreeze():
    """
    Subsequent angle calculations will use whatever the current value of the associated
    constrained angle for the cur- rent mode.
    """
    diffract.unfreeze()

def setlat(
        a: float, b: float, c: float, alpha: float, beta: float, gamma: float
):
    """
    Set sample lattice parameters
    """
    diffract.set_lattice((a, b, c, alpha, beta, gamma))

def setlambda(
        wavelength: float
):
    """
    Set the x-ray wavelength (in Angstroms)
    """
    if wavelength <= 0:
        print('Invalid input: wavelength <=0!')
        return

    current_wavelength = diffract.get_wavelength()
    if math.isclose(wavelength, current_wavelength):
        print(f'Still using {current_wavelength} A')
    else:
        diffract.set_wavelength(wavelength)
        print(f'Lambda reset from {current_wavelength} to {wavelength} A')

def setmode(
        mode: int
):
    """
    Set the geometry mode
    """
    if mode < 0 or mode > 2:
        print('Valid mode is from 0 to 2')
        return

    current_mode = diffract.get_mode()
    if mode == current_mode:
        print(f'Still using mode {current_mode}')
    else:
        diffract.set_mode(mode)
        print(f'Mode reset from {current_mode} to {mode}')

def mvhkl(
        h: float, k: float, l: float, auto=False
):
    """
    Move to the reciprocol space coordinates
    """
    try:
        angles = diffract.forward(h, k, l)[:-2]
    except Exception as exc:
        print(f'{h} {k} {l} is not obtainable: {exc}')
        return

    if not auto:
        for axis, current, target in zip(RealPosition._fields, _currentPosition(), angles):
            print('%7s = %9.4f --> %9.4f' % (axis, current, target))

        answer = input('Move to these values? [Y/n]: ')
        if answer.startswith(('N', 'n')):
            print('Move abandoned.')
            return
        
    br(h, k, l)

def br(
        h: float, k: float, l: float
):
    """
    Move to the reciprocol space coordinates
    """
    args = []
    angles = diffract.forward(h, k, l)[:-2]
    for axis, value in zip(RealPosition._fields, angles):
        motor = getattr(dev, axis)
        args.extend((motor, value))

    scans.mv(*args, relative=False)

def ubr(
        h: float, k: float, l: float
):
    """
    Move to the reciprocol space coordinates with updates
    """
    args = []
    angles = diffract.forward(h, k, l)[:-2]
    for axis, value in zip(RealPosition._fields, angles):
        motor = getattr(dev, axis)
        args.extend((motor, value))

    scans.umv(*args, relative=False)

def umvhkl(
        h: float, k: float, l: float, auto=False
):
    """
    Move to the reciprocol space coordinates with updates
    """
    try:
        angles = diffract.forward(h, k, l)[:-2]
    except Exception as exc:
        print(f'{h} {k} {l} is not obtainable: {exc}')
        return

    if not auto:
        for axis, current, target in zip(RealPosition._fields, _currentPosition(), angles):
            print('%7s = %9.4f --> %9.4f' % (axis, current, target))

        answer = input('Move to these values? [Y/n]: ')
        if answer.startswith(('N', 'n')):
            print('Move abandoned.')
            return
        
    ubr(h, k, l)

def ca(
        h: float, k: float, l: float
):
    """
    Calculate angle positions for a given point in reciprocol space
    """
    angles = diffract.forward(h, k, l)
    print("\nCalculated positions:\n")
    print(f'H K L = {h} {k} {l}')
    print('BetaIn = %.5f  BetaOut = %.5f' %(angles[-2], angles[-1]))
    print('Lambda = %.3f' % diffract.get_wavelength())

    print()
    _showAngles(angles[:-2])

def wh():
    """
    Show where principal axes and reciprocal space
    """
    h = diffract.h.position
    k = diffract.k.position
    l = diffract.l.position
    betaIn = diffract.betaIn.position
    betaOut = diffract.betaOut.position

    print(f'H K L = {h:.4f} {k:.4f} {l:.4f}')
    print('BetaIn = %.5f  BetaOut = %.5f' %(betaIn, betaOut))
    print('Lambda = %.3f' % diffract.get_wavelength())

    print()
    _showAngles()

def pa():
    """
    Show geometry parameters
    """
    if diffract.name == 'x04v':
        print('x04v (Newport Microcontrols 2+3 at SLS) vertical geometry')
    elif diffract.name == 'x04h':
        print('x04h (Newport Microcontrols 2+3 at SLS) horizontal geometry')

    match mode := diffract.get_mode():
        case 0:
            print(f'  BetaIn Fixed (mode {mode})')
        case 1:
            print(f'  BetaOut Fixed (mode {mode})')
        case 2:
            print(f'  BetaIn equals BetaOut (mode {mode})')
    
    if beta_frozen := diffract.get_frozen():
        print(f'  Frozen coordinate: {beta_frozen}')

def orientShow():
    """
    Display list of measured reflections
    """
    print('\n(Using lattice constants:)')
    lattice = diffract.get_lattice()
    print('a = %.4g, b = %.4g, b = %.4g, alpha = %.6g, beta = %.6g, gamma = %.6g' %
          (lattice[0], lattice[1], lattice[2], lattice[3], lattice[4], lattice[5]))

    print("\n------------------------------------------------\n")

    reflections = diffract.get_reflections()
    if reflections:
        _showReflections(reflections)
        print("\n------------------------------------------------\n")

    _showUB()

def orientRemove(
        h: float, k: float, l: float
):
    """
    Remove a measured reflection from the list
    """
    diffract.remove_reflection(h, k, l)

def orientAdd(
        h: float, k: float, l: float, *args
):
    """
    Add a reflection to the list of measured reflections
    """
    angles = args
    if not angles:
        response = diffract.real_position
        # The original return value is of namedtuple type,
        # which gets serialized to a dictionary by the device server.
        angles = tuple(response['values'][axis] for axis in response['fields'] if axis != 'nu')

    if len(angles) < 4:
        print('Please specify all angles')
        return

    diffract.add_reflection(h, k, l, angles)

def orientSave(
        filename: str
):
    """
    Save the current reflections
    """
    configuration = {}
    configuration['geometry'] = diffract.name
    configuration['wavelength'] = diffract.get_wavelength()
    configuration['lattice'] = diffract.get_lattice()
    configuration['reflections'] = diffract.get_reflections()
    
    filepath = pathlib.Path(filename)
    if filepath.exists():
        answer = input('File "%s" already exists. Do you want to overwrite it? [y/N]: ' %(filepath.absolute()))
        if not answer.startswith(('Y', 'y')):
            return

    with open(filepath, 'w') as f:
        json.dump(configuration, f)

def orientLoad(
        filename: str
):
    """
    Load relfections from file
    """
    with open(filename, 'r') as f:
        configuration = json.load(f)
        if configuration['geometry'] != diffract.name:
            print('Saved orientation is for a different geometry "%s", current is "%s".' % configuration['geometry'], diffract.name)
            return

        # save current wavelength, lattice and reflections
        saved_wavelength = diffract.get_wavelength()
        saved_lattice = diffract.get_lattice()
        saved_reflections = diffract.get_reflections()
    
        try:
            diffract.set_lattice(configuration['lattice'])
            
            diffract.clear_reflections()
            for reflection in configuration['reflections']:
                diffract.add_reflection(*reflection)

            _showReflections(configuration['reflections'])
            print("\n------------------------------------------------\n")
            # set wavelength temporarily for orientFit and restore later
            diffract.set_wavelength(configuration['wavelength'])
            orientFit()
        except Exception as exc:
            # restore saved lattice and reflections
            diffract.set_lattice(saved_lattice)
            diffract.clear_reflections()
            for reflection in saved_reflections:
                diffract.add_reflection(*reflection)

            print(exc)
        finally:
            # restore wavelength
            diffract.set_wavelength(saved_wavelength)

def orientFit():
    """
    Fit UB matrix from given reflections
    """
    reflections = diffract.get_reflections()
    if len(reflections) < 2:
        print("There are not enough reflections defined.")
        return

    diffract.compute_UB()
    _showUB()

def ct(exp_time: float):
    """
    Acquire all detectors
    """
    scans.acquire(exp_time=exp_time)

def _showUB():
    UB = diffract.get_UB()
    
    print('Orientation matrix by row:')
    print('  Row 1: %8.5f %8.5f %8.5f' % (UB[0,0], UB[0,1], UB[0,2]))
    print('  Row 2: %8.5f %8.5f %8.5f' % (UB[1,0], UB[1,1], UB[1,2]))
    print('  Row 3: %8.5f %8.5f %8.5f' % (UB[2,0], UB[2,1], UB[2,2]))

def _showAngles(angles=None):
    if angles is None:
        angles = _currentPosition()

    table = PrettyTable(RealPosition._fields, padding=12)
    print(table.get_header())
    text = tuple(f'{x:9.4f}' for x in angles)
    print(table.get_row(*text))

def _currentPosition():
    response = diffract.real_position
    # The original return value is of namedtuple type,
    # which gets serialized to a dictionary by the device server.
    angles = RealPosition(*(response['values'][axis] for axis in response['fields']))
    return angles

def _showReflections(reflections):
    print('The defined reflections are:')
    header = ['h', 'k', 'l'] + diffract.real_position['fields'][:-1]
    table = PrettyTable(header, padding=12)
    print('  ', table.get_header())

    for reflection in reflections:
        h, k, l, angles = reflection
        text = [f'{h:9.4f}', f'{k:9.4f}', f'{l:9.4f}'] + [f'{x:9.4f}' for x in angles]
        print('  ', table.get_row(*text))
