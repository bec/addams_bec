import numpy

from ophyd import PseudoSingle, PseudoPositioner, SoftPositioner,  Component as Cpt, EpicsMotor
from ophyd.signal import AttributeSignal
from ophyd.pseudopos import (real_position_argument, pseudo_position_argument)

from .calc import Mode, RecipCalc, CalcX04V, CalcX04H

class Diffractometer(PseudoPositioner):
    """
    Diffractometer pseudopositioner
    """

    calc_class = None    

    h = Cpt(PseudoSingle, "", kind="hinted")
    k = Cpt(PseudoSingle, "", kind="hinted")
    l = Cpt(PseudoSingle, "", kind="hinted")
    betaIn = Cpt(PseudoSingle, "", kind="hinted")
    betaOut = Cpt(PseudoSingle, "", kind="hinted")

    real_axes = Cpt(AttributeSignal, attr='_real_axes', write_access=False)

    USER_ACCESS = ['forward', 'inverse', 'real_position', 'get_real_positioners',
                   'angles_from_hkls', 'hkls_from_angles',
                   'freeze', 'unfreeze', 'get_frozen',
                   'set_mode', 'get_mode',
                   'set_nu_rot', 'get_nu_rot',
                   'set_lattice', 'get_lattice',
                   'set_wavelength', 'get_wavelength',
                   'get_reflections', 'add_reflection', 'remove_reflection', 'clear_reflections',
                   'compute_UB', 'get_UB'
                   ]

    def __init__(self, prefix, **kwargs):
        self.calc: RecipCalc = self.calc_class()
        self.frozen = False
        self.beta_frozen = [None, None]

        super().__init__(
            prefix,
            **kwargs
        )

        self._real_axes = self.real_positioners._fields
        return

    def freeze(self, angle: float | None):
        self.frozen = True
        match self.calc.mode:
            case Mode.BETAIN_FIX:
                self.beta_frozen[0] = angle if angle is not None else self.betaIn.position
            case Mode.BETAOUT_FIX:
                self.beta_frozen[1] = angle if angle is not None else self.betaOut.position

    def unfreeze(self):
        self.frozen = False

    def get_frozen(self):
        if self.frozen and self.calc.mode != Mode.BETAIN_EQ_BETAOUT:
            return self.beta_frozen[self.calc.mode]

    def get_lattice(self):
        return self.calc.lattice

    def set_lattice(self, lattice):
        self.calc.lattice = lattice

    def set_mode(self, mode):
        self.calc.mode = mode
        # unfreeze if a frozen angle has not been saved for this mode,
        if self.beta_frozen[mode] is None:
            self.unfreeze()

    def get_mode(self):
        return self.calc.mode

    def set_nu_rot(self, nu_rot):
        self.calc.nu_rot = nu_rot

    def get_nu_rot(self):
        return self.calc.nu_rot

    def set_wavelength(self, wavelength):
        self.calc.wavelength = wavelength

    def get_wavelength(self):
        return self.calc.wavelength

    def get_reflections(self):
        return self.calc.reflections

    def add_reflection(self, h, k, l, angles):
        self.calc.add_reflection(h, k, l, angles)

    def remove_reflection(self, h, k, l):
        self.calc.remove_reflection(h, k, l)

    def clear_reflections(self):
        self.calc.clear_reflections()

    def compute_UB(self):
        self.calc.compute_UB()

    def get_UB(self):
        return self.calc.UB

    def get_real_positioners(self):
        return self.RealPosition._fields

    def angles_from_hkls(self, hkls):
        angles = []
        for hkl in hkls:
            real_pos = self.forward(hkl)
            angles.append(real_pos)
        return angles

    def hkls_from_angles(self, angles):
        hkls = []
        for angle in angles:
            psuedo_pos = self.inverse(angle)
            hkls.append(psuedo_pos)
        return hkls

    @pseudo_position_argument
    def forward(self, pseudo_pos):
        hkl = pseudo_pos[:3]
        beta = pseudo_pos[3:]
        if self.frozen:
            beta = self.beta_frozen
        angles, beta = self.calc.angles(*hkl, beta)
        if numpy.isnan(angles.nu):
            angles = angles._replace(nu=self.nu.position)
        return *angles, *beta

    @real_position_argument
    def inverse(self, real_pos):
        # when initialized in bec device server, it does not wait for connection and
        # access the position property with real_pos filled with None.
        if None in real_pos:
            return self.PseudoPosition(numpy.nan, numpy.nan, numpy.nan, numpy.nan, numpy.nan)
        hkl, beta = self.calc.hkl(real_pos)
        return self.PseudoPosition(*hkl, *beta)


class X04V(Diffractometer):
    """
    X04SA diffractometer in vertical geometry
    """
    calc_class = CalcX04V

    ov = Cpt(EpicsMotor, 'X04SA-ES3-XPS:OMEGAV', name='ov')
    alp = Cpt(EpicsMotor, 'X04SA-ES3-XPS:ALPHA', name='alp')
    delta = Cpt(EpicsMotor, 'X04SA-ES3-XPS:DELTA', name='delta')
    gam = Cpt(EpicsMotor, 'X04SA-ES3-XPS:GAMMA', name='gam')
    nu = Cpt(EpicsMotor, 'X04SA-ES3-XPS:NU', name='nu')


class X04H(Diffractometer):
    """
    X04SA diffractometer in horizontal geometry
    """
    calc_class = CalcX04H

    phi = Cpt(EpicsMotor, 'X04SA-ES3-XPS:PHI', name='phi')
    oh = Cpt(EpicsMotor, 'X04SA-ES3-XPS:OMEGAH', name='oh')
    delta = Cpt(EpicsMotor, 'X04SA-ES3-XPS:DELTA', name='delta')
    gam = Cpt(EpicsMotor, 'X04SA-ES3-XPS:GAMMA', name='gam')
    nu = Cpt(EpicsMotor, 'X04SA-ES3-XPS:NU', name='nu')