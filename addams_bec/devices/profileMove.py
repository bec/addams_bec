
from ophyd import (
    EpicsSignal,
    EpicsSignalRO,
    Component as Cpt,
    FormattedComponent as FCpt,
    Device,
    DeviceStatus,
    Kind
)
from ophyd.signal import AttributeSignal

class ProcessSignalStatus(DeviceStatus):
    """
    Status to indicate an operation result.
      * state_signal:  operation is Done(0) or Busy(1)
      * status_signal: operation is Success(1) or Failure(2)
      * status_message: operation message.
    """
    def __init__(self, device, state_signal, status_signal, message_signal, **kwargs):
        self.state_signal = state_signal
        self.status_signal = status_signal
        self.message_signal = message_signal

         # call the base class
        super().__init__(device, **kwargs)

        self.state_signal.subscribe(self._state_callback, event_type=self.state_signal.SUB_VALUE, run=False)

    def _state_callback(self, old_value, value, **kwargs):
        print(self.state_signal.name, old_value, value, self.status_signal.get())
        if old_value != 0 and value == 0:
            self.state_signal.clear_sub(self._state_callback)
            if self.status_signal.get() == 1:
                self.set_finished()
            else:
                self.set_exception(Exception(self.message_signal.get(as_string=True)))


class ProfileMoveAxis(Device):
    use_axis = FCpt(EpicsSignal, '{self.prefix}M{self._motor}UseAxis', kind=Kind.config)
    positions = FCpt(EpicsSignal, '{self.prefix}M{self._motor}Positions', kind=Kind.hinted)
    readbacks = FCpt(EpicsSignal, '{self.prefix}M{self._motor}Readbacks', kind=Kind.hinted)
    following_errors = FCpt(EpicsSignal, '{self.prefix}M{self._motor}FollowingErrors', kind=Kind.hinted)

    def __init__(self, prefix, motor, **kwargs):
        self._motor = motor
        super().__init__(prefix, **kwargs)


class ProfileMoveController(Device):
    USER_ACCESS = ['build_profile', 'execute_profile', 'readback_profile']

    # PVs controlling the number of profile elements and
    # the number of output pulses
    num_axes = Cpt(EpicsSignal, 'NumAxes', kind=Kind.config)
    num_points = Cpt(EpicsSignal, 'NumPoints', kind=Kind.config)
    current_point = Cpt(EpicsSignalRO, 'NumPoints', kind=Kind.hinted)
    num_pulses = Cpt(EpicsSignal, 'NumPulses', kind=Kind.config)
    num_actual_pulses = Cpt(EpicsSignalRO, 'NumActualPulses', kind=Kind.hinted)
    start_pulses = Cpt(EpicsSignal, 'StartPulses', kind=Kind.config)
    end_pulses = Cpt(EpicsSignal, 'EndPulses', kind=Kind.config)

    # PVs controlling the profile speed and acceleration
    time_mode = Cpt(EpicsSignal, 'TimeMode', kind=Kind.config)
    fixed_time = Cpt(EpicsSignal, 'FixedTime', kind=Kind.config)
    times = Cpt(EpicsSignal, 'Times', kind=Kind.config)
    accleration = Cpt(EpicsSignal, 'Acceleration', kind=Kind.config)


    # PV for absolute/relative mode
    move_mode = Cpt(EpicsSignal, 'MoveMode', kind=Kind.config)

    # PVs to build the profile
    build = Cpt(EpicsSignal, 'Build', kind=Kind.config)
    build_state = Cpt(EpicsSignal, 'BuildState', kind=Kind.hinted)
    build_status = Cpt(EpicsSignal, 'BuildStatus', kind=Kind.hinted)
    build_message = Cpt(EpicsSignal, 'BuildMessage', kind=Kind.hinted)

    # PVs to execute and abort the profile
    execute = Cpt(EpicsSignal, 'Execute', kind=Kind.config)
    execute_state = Cpt(EpicsSignal, 'ExecuteState', kind=Kind.hinted)
    execute_status = Cpt(EpicsSignal, 'ExecuteStatus', kind=Kind.hinted)
    execute_message = Cpt(EpicsSignal, 'ExecuteMessage', kind=Kind.hinted)
    abort = Cpt(EpicsSignal, 'Abort', kind=Kind.config)

    # PVs for readback of actual positions and errors
    readback = Cpt(EpicsSignal, 'Readback', kind=Kind.config)
    readback_state = Cpt(EpicsSignal, 'ReadbackState', kind=Kind.hinted)
    readback_status = Cpt(EpicsSignal, 'ReadbackStatus', kind=Kind.hinted)
    readback_message = Cpt(EpicsSignal, 'ReadbackMessage', kind=Kind.hinted)

    # axes
    axes = Cpt(AttributeSignal, attr='_axes', write_access=False)

    def __init__(self, prefix, *args, **kwargs):
        super().__init__(prefix, *args, **kwargs)
        self._axes = self._get_axes()

    def build_profile(self):
        status = ProcessSignalStatus(self, self.build_state, self.build_status, self.build_message)
        self.build.put(1, wait=False)
        return status

    def execute_profile(self):
        status = ProcessSignalStatus(self, self.execute_state, self.execute_status, self.execute_message)
        self.execute.put(1, wait=False)
        return status

    def readback_profile(self):
        status = ProcessSignalStatus(self, self.readback_state, self.readback_status, self.readback_message)
        self.readback.put(1, wait=False)
        return status

    def stop(self):
        self.abort.put(1)

    @classmethod
    def _get_axes(cls):
        axes = []
        for attr, cpt in cls._sig_attrs.items():
            if issubclass(cpt.cls, ProfileMoveAxis):
                axes.append(attr)
        return axes


class ProfileMoveControllerXPS(ProfileMoveController):
    trajectory_file = Cpt(EpicsSignal, 'TrajectoryFile', kind=Kind.config)
    group_name = Cpt(EpicsSignal, 'GroupName', kind=Kind.config)

    def __init__(self, prefix, *args, **kwargs):
        super().__init__(prefix, *args, **kwargs)


class ProfileMoveAxisXPS(ProfileMoveAxis):
    min_position = FCpt(EpicsSignalRO, '{self.prefix}M{self._motor}MinPosition', kind=Kind.hinted)
    max_position = FCpt(EpicsSignalRO, '{self.prefix}M{self._motor}MaxPosition', kind=Kind.hinted)
    max_velocity = FCpt(EpicsSignalRO, '{self.prefix}M{self._motor}MaxVelocity', kind=Kind.hinted)
    max_acceleration = FCpt(EpicsSignalRO, '{self.prefix}M{self._motor}MaxAcceleration', kind=Kind.hinted)

    def __init__(self, prefix, motor, **kwargs):
        super().__init__(prefix, motor, **kwargs)


class ProfileMoveXPSX04SA(ProfileMoveControllerXPS):
    alp = Cpt(ProfileMoveAxisXPS, '', motor='1')
    delta = Cpt(ProfileMoveAxisXPS, '', motor='2')
    gam = Cpt(ProfileMoveAxisXPS, '', motor='3')
    ov = Cpt(ProfileMoveAxisXPS, '', motor='4')
    xv = Cpt(ProfileMoveAxisXPS, '', motor='5')
    phi = Cpt(ProfileMoveAxisXPS, '', motor='6')
    oh = Cpt(ProfileMoveAxisXPS, '', motor='7')
    nu = Cpt(ProfileMoveAxisXPS, '', motor='8')
