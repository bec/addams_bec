from ophyd import (
    ADComponent as ADCpt,
    Device,
    DeviceStatus,
)

from ophyd.areadetector import ADBase, register_plugin

from ophyd_devices.devices.areadetector.cam import SLSDetectorCam
from ophyd_devices.devices.areadetector.plugins import (
    PluginBase_V35 as PluginBase,
    ImagePlugin_V35 as ImagePlugin,
    StatsPlugin_V35 as StatsPlugin,
    HDF5Plugin_V35 as HDF5Plugin,
    ROIPlugin_V35 as ROIPlugin,
    ROIStatPlugin_V35 as ROIStatPlugin,
    ROIStatNPlugin_V35 as ROIStatNPlugin,
)
from ophyd_devices.interfaces.base_classes.psi_detector_base import PSIDetectorBase, CustomDetectorMixin

from bec_lib import bec_logger, messages
from bec_lib.endpoints import MessageEndpoints

logger = bec_logger.logger

DETECTOR_TIMEOUT = 5

class Eiger500KSetup(CustomDetectorMixin):
    def __init__(self, *args, parent:Device = None, **kwargs):
        super().__init__(*args, parent=parent, **kwargs)
        self._counter = 0

    def on_stage(self):
        exposure_time  = self.parent.scaninfo.exp_time
        num_points = self.parent.scaninfo.num_points

        # camera acquisition parameters
        self.parent.cam.array_counter.put(0)
        if self.parent.scaninfo.scan_type == 'step':
            self.parent.cam.acquire_time.put(exposure_time)
            self.parent.cam.num_images.put(1)
            self.parent.cam.image_mode.put(0)  # Single
            self.parent.cam.trigger_mode.put(0) # auto
        else:
            # In flyscan, the exp_time is the time between two triggers,
            # which minus 15% is used as the acquisition time.
            self.parent.cam.acquire_time.put(exposure_time * 0.85)
            self.parent.cam.num_images.put(num_points)
            self.parent.cam.image_mode.put(1)  # Multiple
            self.parent.cam.trigger_mode.put(1) # trigger
            self.parent.cam.acquire.put(1, wait=False) # arm

        # file writer
        self.parent.hdf.lazy_open.put(1)
        self.parent.hdf.num_capture.put(num_points)
        self.parent.hdf.file_write_mode.put(2) # Stream
        self.parent.hdf.capture.put(1, wait=False)
        self.parent.hdf.enable.put(1) # enable plugin

        # roi statistics to collect signal and background in a timeseries
        self.parent.roistat.enable.put(1)
        self.parent.roistat.ts_num_points.put(num_points)
        self.parent.roistat.ts_control.put(0, wait=False) # Erase/Start

        logger.success('XXXX stage XXXX')

    def on_trigger(self):
        self.parent.cam.acquire.put(1, wait=False)
        logger.success('XXXX trigger XXXX')

        return self.wait_with_status(
            [(self.parent.cam.acquire.get, 0)],
            self.parent.scaninfo.exp_time + DETECTOR_TIMEOUT,
            all_signals=True
        )

    def on_complete(self):
        status = DeviceStatus(self.parent)

        if self.parent.scaninfo.scan_type == 'step':
            timeout = DETECTOR_TIMEOUT
        else:
            timeout = self.parent.scaninfo.exp_time * self.parent.scaninfo.num_points + DETECTOR_TIMEOUT
        logger.success('XXXX %s XXXX' % self.parent.roistat.ts_acquiring.get())
        success = self.wait_for_signals(
            [
                (self.parent.cam.acquire.get, 0),
                (self.parent.hdf.capture.get, 0),
                (self.parent.roistat.ts_acquiring.get, 'Done')
            ],
            timeout,
            check_stopped=True,
            all_signals=True
        )

        # publish file location
        self.parent.filepath.put(self.parent.hdf.full_file_name.get())
        self.publish_file_location(done=True, successful=success)

        # publish timeseries data
        metadata = self.parent.scaninfo.scan_msg.metadata
        metadata.update({"async_update": "append", "num_lines": self.parent.roistat.ts_current_point.get()})
        msg = messages.DeviceMessage(
            signals={
                self.parent.roistat.roi1.name_.get(): {
                    'value': self.parent.roistat.roi1.ts_total.get(),
                },
                self.parent.roistat.roi2.name_.get(): {
                    'value': self.parent.roistat.roi2.ts_total.get(),
                },
            },
            metadata=self.parent.scaninfo.scan_msg.metadata
        )
        self.parent.connector.xadd(
            topic=MessageEndpoints.device_async_readback(
                scan_id=self.parent.scaninfo.scan_id, device=self.parent.name
            ),
            msg_dict={"data": msg},
            expire=1800,
        )

        logger.success('XXXX complete %d XXXX' % success)
        if success:
            status.set_finished()
        else:
            status.set_exception(TimeoutError())
        return status

    def on_stop(self):
        logger.success('XXXX stop XXXX')
        self.parent.cam.acquire.put(0)
        self.parent.hdf.capture.put(0)
        self.parent.roistat.ts_control.put(2)

    def on_unstage(self):
        self.parent.cam.acquire.put(0)
        self.parent.hdf.capture.put(0)
        self.parent.hdf.enable.put(0) # disable plugin
        self.parent.roistat.ts_control.put(2)
        logger.success('XXXX unstage XXXX')


@register_plugin
class BadPixelPlugin(PluginBase, version=(3, 11, 0), version_type="ADCore"):
    _default_suffix = "BadPix1:"
    _suffix_re = r"BadPix\d:"
    _html_docs = ["NDPluginBadPixel.html"]
    _plugin_type = "NDPluginBadPixel"


class EigerROIStatPlugin(ROIStatPlugin):
    roi1 = ADCpt(ROIStatNPlugin, '1:')
    roi2 = ADCpt(ROIStatNPlugin, '2:')

class Eiger500K(PSIDetectorBase, ADBase):
    """
    """
    custom_prepare_cls = Eiger500KSetup

    cam = ADCpt(SLSDetectorCam, 'cam1:')

    #image = ADCpt(ImagePlugin, 'image1:')
    #roi1 = ADCpt(ROIPlugin, 'ROI1:')
    #roi2 = ADCpt(ROIPlugin, 'ROI2:')
    #stats1 = ADCpt(StatsPlugin, 'Stats1:')
    #stats2 = ADCpt(StatsPlugin, 'Stats2:')
    badpix = ADCpt(BadPixelPlugin, 'BadPix1:')
    roistat = ADCpt(EigerROIStatPlugin, 'ROIStat1:')
    #roistat1 = ADCpt(ROIStatNPlugin, 'ROIStat1:1:')
    #roistat2 = ADCpt(ROIStatNPlugin, 'ROIStat1:2:')
    hdf = ADCpt(HDF5Plugin, 'HDF1:')