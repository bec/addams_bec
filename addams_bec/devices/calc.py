"""
Angular calculation for X04SA surface diffractometer.

The calculation is detailed in J. Appl. Cryst. 44, 73-83 (2011).
The SPEC implemention in C is at https://git.psi.ch/spec/spec/-/tree/master/sls.

The SPEC functions, i.e. atoQ, Qtoa and ang_to_hphi, are converted to Python.
The C code structrure and comments are kept whenever possible.
"""
import collections
import enum
from math import pi, sin, cos, tan, asin, acos, atan, atan2, fabs, sqrt
from typing import Tuple

import numpy

RAD = pi / 180   # convert degree to radian
RAD_1 = 180 / pi # convert radian to degree
VERY_SMALL = 1e-15
TWOPI = 2 * pi

class Mode(enum.IntEnum):
    """
    Incidience and emergence angle modes
    """
    BETAIN_FIX = 0 #: given betaIn, find others
    BETAOUT_FIX = 1 #: given betaOut, find others
    BETAIN_EQ_BETAOUT = 2 #: betaIn equals betaOut


class NuMode(enum.IntEnum):
    """
    Nu motor modes
    """
    NO = 0 #: no rotation
    L_FIX = 1 #: static l-projection angle on the pixel detector
    FOOTPRINT_FIX = 2 #: static footprint-projection on the pixel detector


class Crystal:
    """
    Crystal lattic parameters
    """
    def __init__(self, a1: float, a2: float, a3: float,
                 alpha1: float, alpha2: float, alpha3: float):
        self.lattice = (a1, a2, a3, alpha1, alpha2, alpha3)

    @property
    def lattice(self):
        return self.a1, self.a2, self.a3, self.alpha1, self.alpha2, self.alpha3

    @lattice.setter
    def lattice(self, new_lattice):
        self.a1, self.a2, self.a3, self.alpha1, self.alpha2, self.alpha3 = new_lattice
        self._compute_B()

    def _compute_B(self):
        alpha1 = self.alpha1 * RAD
        alpha2 = self.alpha2 * RAD
        alpha3 = self.alpha3 * RAD

        # Calculate the reciprocal lattice parameters
        beta2 = acos((cos(alpha1) * cos(alpha3) - cos(alpha2)) / (sin(alpha1) * sin(alpha3)))

        beta3 = acos((cos(alpha1) * cos(alpha2) - cos(alpha3)) / (sin(alpha1) * sin(alpha2)))

        volume = self.a1 * self.a2 * self.a3 * \
                 sqrt(1 + 2 * cos(alpha1) * cos(alpha2) * cos(alpha3)
                      - cos(alpha1) ** 2 - cos(alpha2) ** 2 - cos(alpha3) ** 2)

        b1 = 2 * pi * self.a2 * self.a3 * sin(alpha1) / volume
        b2 = 2 * pi * self.a1 * self.a3 * sin(alpha2) / volume
        b3 = 2 * pi * self.a1 * self.a2 * sin(alpha3) / volume


        # Calculate the BMatrix from the direct and reciprocal parameters.
        # Reference: Eq.3 of Busing and Levy, Acta Cryst. 22, 457-464 (1967).
        self.B = numpy.array([
                [b1, b2 * cos(beta3), b3 * cos(beta2)],
                [0.0, b2 * sin(beta3), -b3 * sin(beta2) * cos(alpha1)],
                [0.0, 0.0, 2 * pi / self.a3],
        ])

    def __str__(self):
        return f"a = {self.a1}, b = {self.a2}, c = {self.a3}, alpha = {self.alpha1}, beta = {self.alpha2}, gamma = {self.alpha3} "


class Geometry:
    """
    A diffractometer interface to convert between reciprocol space and real space.
    """

    @property
    def Position(self):
        return collections.namedtuple("Position", ','.join(self.axes))

    def Qtoa(self,
             hphi: Tuple[float, float, float],
             beta: Tuple[float, float],
             wavelength: float) -> Tuple[Tuple[float, float, float, float, float], Tuple[float, float]]:
        """
        Convert from reciprocol space to angles.
        """
        if wavelength <= 0:
            raise ValueError("Incident wave-length is <=0")

        q = TWOPI / wavelength
        hphi /= q

        return self.hphi_to_ang(hphi, beta)

    def atoQ(self,
             a: Tuple[float, float, float, float], 
             wavelength: float) -> Tuple[Tuple[float, float, float], Tuple[float, float]]:
        """
        Convert from angles to reciprocal space
        
        :param a: diffractometer angles
        :param beta: incidence and emergence angles
        :return: (h,k,l), (betaIn, betaOut)
        """
        if wavelength <= 0:
            raise ValueError("Incident wave-length is <=0")

        # calculate corresponding reciprocal space coordinates
        hphi = self.ang_to_hphi(a)
        
        # calculate incidence and emergence angles
        _, _, l = hphi
        BETAIN = a[1]
        sbi = sin(BETAIN * RAD)
        sbo = l - sbi
        if fabs(sbo) > 1:
            raise ValueError("|sin(betaOut)| > 1 --> betaOut > 90 deg!")
        BETAOUT = asin(sbo) * RAD_1    # betaOut in deg

        q = TWOPI / wavelength
        hphi *= q

        return hphi, (BETAIN, BETAOUT)

    def hphi_to_ang(self, hphi: Tuple[float, float, float], beta: Tuple[float, float]):
        """
        Convert from orthonormal cartesian coordinates in reciprocal space to angles
        """
        raise NotImplementedError()

    def ang_to_hphi(self, angles: Tuple[float, float, float, float, float]):
        """
        Create normalized momentum transfer Qphi from spectrometer angles.
        """
        raise NotImplementedError()

    def _locate(self, a, min):
        """
        Cut points for angles
        """
        if a < min:
            a += 360
        return a


class GeometryX04V(Geometry):
    """
    X04SA SD vertical geometry
    """

    axes = ('ov', 'alp', 'delta', 'gam', 'nu')

    def __init__(self):
        self.mode = Mode.BETAIN_FIX
        self.nu_rot = NuMode.NO

    def hphi_to_ang(self, hphi, beta):
        h, k, l = hphi
        BETAIN, BETAOUT = beta

        h2 = h * h
        k2 = k * k
        l2 = l * l

        # calculate Y (common for all modes)
        Y = -0.5 * (h2 + k2 + l2)

        # calculate BETAIN and BETAOUT depending on the mode
        match self.mode:
            case Mode.BETAIN_FIX:
                sbi = sin(BETAIN * RAD)

                sbo = l - sbi
                if fabs(sbo) > 1:
                    raise ValueError("|sin(betaOut)| > 1 --> betaOut > 90 deg!")
                BETAOUT = asin(sbo) * RAD_1    # betaOut in deg

            case Mode.BETAOUT_FIX:
                sbo = sin(BETAOUT * RAD)

                sbi = l - sbo
                if fabs(sbi) > 1:
                    raise ValueError("|sin(betaIn)| > 1 --> incidence angle > 90 deg!")
                BETAIN = asin(sbi) * RAD_1

            case Mode.BETAIN_EQ_BETAOUT:
                sbi = l / 2
                if fabs(sbi) > 1:
                    raise ValueError("|L/2| > 1")

                sbo = sbi
                BETAIN = BETAOUT = asin(sbi) * RAD_1


        cbi = cos(BETAIN * RAD)
        if fabs(cbi) < VERY_SMALL:
            raise ValueError("cos(betaIn) = 0 --> angle of incidence is 90 deg!")

        # calculate Z and X based on BETAIN and BETAOUT */

        sa = sbi
        alp = asin(sbi)
        ca = cos(alp)

        Z  = (((Y + 1) * sbi + sbo) / ca)
        A  = (ca * Y + sa * Z)
        A2 = A * A

        if (h2 + k2 - A2) < VERY_SMALL:
            raise ValueError("X is NaN")

        X  = sqrt(h2 + k2 - A2)
        X2 = X * X

        # calculate angles

        # gam
        if fabs(Z) < VERY_SMALL and fabs(Y+1) < VERY_SMALL:
            raise ValueError("gam = atan2(0,0)")
        gam = atan2(Z,(Y+1))

        # del
        sd = X
        if fabs(sin(gam)) < VERY_SMALL:
            raise ValueError("cos(del) is infinite")
        cd = Z / sin(gam)
        if fabs(sd) < VERY_SMALL and fabs(cd) < VERY_SMALL:
            raise ValueError("del = atan2(0,0)")
        delta = atan2(sd,cd)

        # ov
        sov = (k * X - h * A) / (A2 + X2)
        cov = (h * X + k * A) / (A2 + X2)
        if fabs(sov) < VERY_SMALL and fabs(cov) < VERY_SMALL:
            raise ValueError("ov = atan2(0,0)")
        ov  = atan2(sov,cov)

        # alp, nu
        alp = asin(sbi)
        nu = numpy.nan # set new nu angle only when nu is allowed to rotate
        if self.nu_rot == NuMode.L_FIX:
            nu  = atan(-tan(gam - alp) * sin(delta))
        elif self.nu_rot == NuMode.FOOTPRINT_FIX:
            if fabs(sd) < VERY_SMALL and fabs(sin(gam - alp)) < VERY_SMALL:
                nu = 0.0
            else:
                nu  = atan2(sd*cos(gam - alp),sin(gam - alp))

        GAM = gam   * RAD_1
        DEL = delta * RAD_1
        ALP = alp   * RAD_1
        OV  = ov    * RAD_1
        NU  = nu    * RAD_1

        # cut points for angles
        GAM = self._locate(GAM, -90)
        DEL = self._locate(DEL, -90)
        ALP = self._locate(ALP, -90)
        OV = self._locate(OV, -180)
        NU = self._locate(NU,  -90)

        return self.Position(OV, ALP, DEL, GAM, NU), (BETAIN, BETAOUT)


    def ang_to_hphi(self, angles):
        """
        Create normalized momentum transfer Qphi from spectrometer angles.
        """
        a = numpy.array(angles, dtype=float)
        a *= RAD

        cov = cos(a[0]);    sov = sin(a[0])
        ca  = cos(a[1]);    sa  = sin(a[1])
        cd  = cos(a[2]);    sd  = sin(a[2])
        cg  = cos(a[3]);    sg  = sin(a[3])

        X = sd
        Y = (cd * cg) - 1
        Z = cd * sg

        hphi = numpy.zeros(3)
        hphi[0] = X * cov - (Y * ca + Z * sa) * sov
        hphi[1] = X * sov + (Y * ca + Z * sa) * cov
        hphi[2] = Z * ca  - Y * sa

        return hphi


class GeometryX04H(Geometry):
    axes = ('phi', 'oh', 'delta', 'gam', 'nu')

    def __init__(self):
        self.mode = Mode.BETAIN_FIX
        self.nu_rot = NuMode.NO

    def hphi_to_ang(self, hphi, beta):
        """
        Convert from orthonormal cartesian coordinates in reciprocal space to angles
        """
        h, k, l = hphi
        BETAIN, BETAOUT = beta

        h2 = h * h
        k2 = k * k
        l2 = l * l

        # Eq. 45: calculate X (common for all modes)
        Y = -0.5 * (h2 + k2 + l2)

        # Eq. 46: calculate BETAIN and BETAOUT depending on the mode
        match self.mode:
            case Mode.BETAIN_FIX:
                sbi = sin(BETAIN * RAD)
                sbo = l - sbi
                if fabs(sbo) > 1:
                    raise ValueError("|sin(betaOut)| > 1 --> betaOut > 90 deg!")

                BETAOUT = asin(sbo) * RAD_1;    # betaOut in deg

            case Mode.BETAOUT_FIX:
                sbo = sin(BETAOUT * RAD)
                sbi = l - sbo
                if fabs(sbi) > 1:
                    raise ValueError("|sin(betaIn)| > 1 --> incidence angle > 90 deg!")

                BETAIN = asin(sbi) * RAD_1

            case Mode.BETAIN_EQ_BETAOUT:
                sbi = l / 2
                if fabs(sbi) > 1:
                    raise ValueError("|L/2| > 1")

                sbo = sbi
                BETAIN = BETAOUT = asin(sbi) * RAD_1

        cbi = cos(BETAIN * RAD)
        if fabs(cbi) < VERY_SMALL:
            raise ValueError("cos(betaIn) = 0 --> angle of incidence is 90 deg!")

        # Eq. 47-48: calculate Z and X based on BETAIN and BETAOUT
        soh = sbi
        oh  = asin(sbi)
        coh = cos(oh)
        Z   = (((Y + 1) * sbi + sbo) / coh)
        A   = (cbi * Y + sbi * Z)
        A2  = A * A

        if h2 + k2 - A2 < VERY_SMALL:
            raise ValueError("Y is NaN")
        X  = -sqrt(h2 + k2 - A2)
        X2 = X * X

        # calculate angles

        # Eq. 49: gam
        sg = -X
        cg = Y + 1
        if fabs(X) < VERY_SMALL and fabs(Y+1) < VERY_SMALL:
            raise ValueError("gam = atan2(0,0)")
        gam = atan2(-X,(Y+1))

        # Eq. 50: delta
        sd  = Z
        cd  = -X / sin(gam)
        if fabs(sd) < VERY_SMALL and fabs(cd) < VERY_SMALL:
            raise ValueError("del = atan2(0,0)")
        delta = atan2(sd,cd)

        # Eq. 51: phi
        sp  = (h * A - k * X) / (A2 + X2)
        cp  = (h * X + k * A) / (A2 + X2)
        if fabs(sp) < VERY_SMALL and fabs(cp) < VERY_SMALL:
            raise ValueError("phi = atan2(0,0)")
        phi = atan2(sp,cp)

        # Eq. 52: oh
        oh = asin(sbi)

        # Eq. nu
        nu = numpy.nan
        if self.nu_rot == NuMode.L_FIX:
            if (fabs(-sg * soh) < VERY_SMALL and
                fabs(soh * cg * sd + coh * cd) < VERY_SMALL):
                nu = 0.0
            else:
                nu  = atan2(-sg * soh , soh * cg * sd + coh * cd)
        elif self.nu_rot == NuMode.FOOTPRINT_FIX:
            if (fabs(cg * sin(delta - oh)) < VERY_SMALL and 
                fabs(sg) < VERY_SMALL):
                nu = 0.0
            else:
                nu  = atan2(cg*sin(delta - oh),sg)

        PHI = phi   * RAD_1
        OH  = oh    * RAD_1
        GAM = gam   * RAD_1
        DEL = delta * RAD_1
        NU  = nu * RAD_1

        # cut points for angles
        PHI = self._locate(PHI, -180)
        OH  = self._locate(OH,  -90)
        DEL = self._locate(DEL, -90)
        GAM = self._locate(GAM, -90)
        NU  = self._locate(NU,  -90)

        return self.Position(PHI, OH, DEL, GAM, NU), (BETAIN, BETAOUT)

    def ang_to_hphi(self, angles):
        """
        Create normalized momentum transfer Qphi from spectrometer angles.
        """
        a = numpy.array(angles)
        a *= RAD

        cp  = cos(a[0]);    sp  = sin(a[0])
        coh = cos(a[1]);    soh = sin(a[1])
        cd  = cos(a[2]);    sd  = sin(a[2])
        cg  = cos(a[3]);    sg  = sin(a[3])

        # Eq. 43
        X = -cd * sg
        Y = cd * cg -1
        Z = sd

        # Eq. 44
        hphi = numpy.zeros(3)
        hphi[0] =  cp * X + sp * (coh * Y + soh * Z)
        hphi[1] = -sp * X + cp * (coh * Y + soh * Z) 
        hphi[2] = coh * Z - soh * Y

        return hphi


class RecipCalc:
    geometry_class: Geometry = None

    def __init__(self):
        self._geometry: Geometry =  self.geometry_class()
        self._crystal = Crystal(TWOPI, TWOPI, TWOPI, 90, 90, 90)

        self._wavelength = 1
        self.reflections = []
        self.U = self.UB = self.UBinv = numpy.identity(3)

    @property
    def mode(self):
        return self._geometry.mode

    @mode.setter
    def mode(self, mode):
        self._geometry.mode = mode

    @property
    def nu_rot(self):
        return self._geometry.nu_rot

    @nu_rot.setter
    def nu_rot(self, nu_rot):
        self._geometry.nu_rot = nu_rot

    @property
    def lattice(self):
        return self._crystal.lattice

    @lattice.setter
    def lattice(self, lattice):
        self._crystal.lattice = lattice
        self._update_UB()

    @property
    def wavelength(self):
        return self._wavelength
    
    @wavelength.setter
    def wavelength(self, wavelength):
        self._wavelength = wavelength

    def _update_UB(self):
        if self.U is not None:
            self.UB = self.U @ self._crystal.B
            self.UBinv = numpy.linalg.inv(self.UB)

    def angles(self, h: float, k: float, l: float, beta: Tuple[float, float]):
        """
        Calculate angles from H, K and L.
        """
        hh = numpy.zeros(3)      # scattering vector in lattice units
        hphi = numpy.zeros(3)    # scattering vector in orthonormal cartesian coordinates
                                 # in reciprocal space

        hh[0] = h
        hh[1] = k
        hh[2] = l

        # Convert from crystal lattice to orthonormal cartesian coordinates
        # in reciprocal space by applying the UB matrix to (H K L)
        hphi = numpy.matmul(self.UB, hh)

        # Angles from HKL
        angles = self._geometry.Qtoa(hphi, beta, self._wavelength)
        return angles

    def hkl(self, angles: Tuple[float, float, float, float]) -> Tuple[Tuple[float, float, float], Tuple[float, float]]:
        """
        Calculate H, K and L from angles.
        """
        # Convert from angles to orthonormal cartesian coordinates in reciprocal space
        hphi, beta = self._geometry.atoQ(angles, self._wavelength)

        # Convert from orthonormal cartesian coordinates in reciprocal
        # space to the crystal lattice positions by applying the inverse
        # of the UB matrix on hphi = (h' k' l')
        hh = numpy.matmul(self.UBinv, hphi)
        return hh, beta

    def add_reflection(self, h: float, k: float, l: float, angles: Tuple[float, float, float, float]):
        """
        Add a reflection to the list. If the reflection already exists, replace it with the new angles.
        """
        for i in range(len(self.reflections)):
            if numpy.allclose(self.reflections[i][:3], (h, k, l)):
                self.reflections[i] = (h, k, l, angles)
                return
        self.reflections.append((h, k, l, angles))

    def remove_reflection(self, h: float, k: float, l: float):
        """
        Remove a reflection from the list.
        """
        for i in range(len(self.reflections)):
            if numpy.allclose(self.reflections[i][:3], (h, k, l)):
                del self.reflections[i]

    def clear_reflections(self):
        """
        Remove all reflections
        """
        self.reflections.clear()

    def compute_UB(self):
        """
        """
        # Hp = U @ Hc
        Hc = numpy.empty(shape=(3,0)) # in crystal system
        Hp = numpy.empty(shape=(3,0)) # in phi system

        for ref in self.reflections:
            h, k, l, angles = ref
            Hc = numpy.c_[Hc, self._crystal.B @ [h, k, l]]
            Hp = numpy.c_[Hp, self._geometry.atoQ(angles, self._wavelength)[0]]

        if len(self.reflections) == 2:
            # Busing & Levy, Acta Cryst. (1967). 22, 457
            h1c = Hc[:,0]
            h2c = Hc[:,1]
            u1p = Hp[:,0]
            u2p = Hp[:,1]

            # Create modified unit vectors t1, t2 and t3 in crystal and phi systems
            t1c = h1c
            t3c = numpy.cross(h1c, h2c)
            t2c = numpy.cross(t3c, t1c)

            t1p = u1p
            t3p = numpy.cross(u1p, u2p)
            t2p = numpy.cross(t3p, t1p)

            # ...and nornmalise and check that the reflections used are appropriate
            def __normalise(m):
                d = numpy.linalg.norm(m, axis=0)
                if any(d < 1e-7):
                    return
                return m / d

            Tc = __normalise(numpy.column_stack((t1c, t2c, t3c)))
            Tp = __normalise(numpy.column_stack((t1p, t2p, t3p)))

            U = Tp @ numpy.linalg.inv(Tc)
        elif len(self.reflections) == 3: # determined system 
            U = Hp @ numpy.linalg.inv(Hc)
        elif len(self.reflections) > 3:  # over-determined system
            U = numpy.transpose((numpy.linalg.inv(Hc @ Hc.T) @ Hc ) @ Hp.T)  # linear least squares

        self.U = U
        self._update_UB()


class CalcX04V(RecipCalc):
    geometry_class = GeometryX04V


class CalcX04H(RecipCalc):
    geometry_class = GeometryX04H
